#include "Polygon.h"
#include <string>

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name);
	virtual ~Triangle();
	virtual void clearDraw(const Canvas& canvas);
	virtual void draw(const Canvas& canvas);
	virtual double getArea() const;
	virtual double getPerimeter() const ;
	bool getIsValid() const;
	virtual void move(const Point& other);
	int getX();
	int getY();

	// override functions if need (virtual + pure virtual)
private:
	bool is_valid;
};