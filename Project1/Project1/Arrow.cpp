#include "Arrow.h"

Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name):Shape(name, type)
{
	this->_points.resize(2); //init the vector
	this->_points[0] = new Point(a);
	this->_points[1] = new Point(b);
}

Arrow::~Arrow()
{
	delete &this->_points;
}

void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(*_points[0], *_points[1]);
}
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(*_points[0], *_points[1]);
}

double Arrow::getArea() const
{
	return 0; //not implemented yet..
}

double Arrow::getPerimeter() const
{
	return this->_points[0]->distance(*this->_points[1]);
}

void Arrow::move(const Point& other)
{
	*this->_points[0]  += other;
	*this->_points[1] += other;

}

int Arrow::getX()
{
	return this->_points[0]->getX();
}

int Arrow::getY()
{
	return this->_points[0]->getY();
}





