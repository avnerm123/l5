#pragma once
#include "Shape.h"
#include "Canvas.h"
#include <vector>

#define EXIT 3
#define RMV_CHOICE 2
#define MOV 0
#define DETAILS 1
class Menu
{
public:

	Menu();
	~Menu();
	void addShape();
	void deleteAllShapes();
	void show();
	// more functions..

private: 
	Canvas _canvas;
	std::vector<Shape*> shapes;
};

