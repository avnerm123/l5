#include "Point.h"
#include <iostream>
#include <cmath>

Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

Point::Point(const Point& other)
{
	this->_x = other.getX();
	this->_y = other.getY();
}

Point::~Point()
{
}

Point Point::operator+(const Point& other) const
{
	return Point(this->_x + other.getX(), this->_y - other.getY());
	
}

Point& Point::operator+=(const Point& other)
{
	// TODO: insert return statement here
	this->_x += other.getX();
	this->_y += other.getY();
	return *this;
}

double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}

double Point::distance(const Point& other) const
{
	return std::abs(std::sqrt(((this->_x - other.getX())*(this->_x - other.getX())) - ((this->_y - other.getY()))*(this->_y - other.getX())));
}
