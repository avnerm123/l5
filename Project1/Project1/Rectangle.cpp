#include "Rectangle.h"
#include <iostream>



void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(*_points[0], Point(_points[0]->getX() + this->_length , _points[0]->getY() - this->_width));
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(*_points[0], Point(_points[0]->getX() + this->_length, _points[0]->getY() - this->_width));
}

myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name):Polygon(name, type)
{
	this->_points.resize(5);
	if (length < 0)
	{
		do {
			std::cout << "The length can't be nagetive, Enter the length again" << std::endl;
			std::cin >> length;
		} while (length < 0);
	}
	if (width < 0)
	{
		do {
			std::cout << "The width can't be nagetive, Enter the width again" << std::endl;
			std::cin >> width;
		} while (width < 0);
	}


	this->_points[0] = new Point(a);
	this->_length = length;
	this->_width = width;

}

myShapes::Rectangle::~Rectangle()
{
	delete &this->_points;
}

double myShapes::Rectangle::getArea() const
{
	return this->_length * this->_width; //S = L*W
}

double myShapes::Rectangle::getPerimeter() const
{
	return 2*this->_length + 2*this->_width; // P = 2*l + 2*w
}

void myShapes::Rectangle::move(const Point& other)
{
	*this->_points[0] += other;
}

int myShapes::Rectangle::getX()
{
	return this->_points[0]->getX();
}

int myShapes::Rectangle::getY()
{
	return this->_points[0]->getY();
}
