#include "Circle.h"
#include <iostream>


Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name):Shape(name, type), _center(center)
{	
	if (radius < 0)
	{
		do {
			std::cout << "Radius can't be nagetive, Enter the radius again" << std::endl;
			std::cin >> radius;
		} while (radius < 0);
	}
	this->_radius = radius;
}

Circle::~Circle()
{
}

const Point& Circle::getCenter() const
{
	// TODO: insert return statement here
	return this->_center;
}

double Circle::getRadius() const
{
	return this->_radius;
}

void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}

double Circle::getArea() const
{
	return (PI * this->_radius) * (PI * this->_radius); //S = Pi*R^2
}

double Circle::getPerimeter() const
{
	return 2 * PI * this->_radius; //P = Pi*2*R
}

void Circle::move(const Point& other)
{
	this->_center = other;
}

int Circle::getX()
{
	return this->_center.getX();
}

int Circle::getY()
{
	return this->_center.getY();
}


