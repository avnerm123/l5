#include "Triangle.h"
#include <iostream>



void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(*_points[0], *_points[1], *_points[2]);
}

double Triangle::getArea() const
{
	double result1 = this->_points[0]->getX() * (this->_points[1]->getY() - this->_points[2]->getY());
	double result2 = this->_points[1]->getX() * (this->_points[2]->getY() - this->_points[0]->getY());
	double result3 = this->_points[2]->getX() * (this->_points[0]->getY() - this->_points[1]->getY());
	return (result1 + result2 + result3)/2; // S = (ax(bx - cx) +  bx(ax - cx) + cx(ax - bx))/2
}

double Triangle::getPerimeter() const
{
	double result1 = this->_points[0]->distance(*this->_points[1]);
	double result2 = this->_points[0]->distance(*this->_points[2]);
	double result3 = this->_points[1]->distance(*this->_points[3]);
	return result1 + result2 + result3; // P = a + b + c
}

bool Triangle::getIsValid() const
{
	return this->is_valid;
}

void Triangle::move(const Point& other)
{
	*this->_points[0] += other;
	*this->_points[1] += other;
	*this->_points[2] += other;
}

int Triangle::getX()
{
	return this->_points[0]->getX();
}

int Triangle::getY()
{
	return this->_points[0]->getY();
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(*_points[0], *_points[1], *_points[2]);
}

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name):Polygon(name, type)
{
	this->is_valid = true;
	this->_points.resize(5); //init the vector
	if ((a.getY() == b.getY() && b.getY() == c.getY()) || (a.getX() == b.getX() && b.getX() == c.getX()))
	{
		std::cout << "All the points are on the same line! try again";
		this->is_valid = false;
	}



	this->_points[0] = new Point(a.getX(), a.getY());
	this->_points[1] = new Point(b.getX(), b.getY()); 
	this->_points[2] = new Point(c.getX(), c.getY());
}

Triangle::~Triangle()
{
	delete &this->_points;
}
