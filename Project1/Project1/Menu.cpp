#include "Menu.h"
#include "Arrow.h"
#include <iostream>
#include "Circle.h"
#include "Triangle.h"
#include "Rectangle.h"

Menu::Menu() 
{
	
	int choice = 0;
	while (choice != EXIT)
	{
		do {
			system("cls");
			std::cout << "Enter 0 to add a new shape." << std::endl;
			std::cout << "Enter 1 to modify or get information from a current shape." << std::endl;
			std::cout << "Enter 2 to delete all shapes." << std::endl;
			std::cout << "Enter 3 to exit" << std::endl;
			std::cin >> choice;
			system("cls");
		} while (choice < 0 || choice > EXIT); //get the users choice 0 - 3

		switch (choice)
		{
		case 0:
			addShape();
			break;
		case 1:
			if (this->shapes.size() > 0) //cann't modify or get information from a shape if there are no shapes yet
			{
				show();
			}
			else
			{
				std::cout << "No shapes";
			}
			break;
		case 2:
			deleteAllShapes();
			break;
		case 3:
			break;
		default:
			break;
		}

		for (int i = 0; i < this->shapes.size(); i++) //drawing all the shapes
		{
			this->shapes[i]->draw(this->_canvas);
		}
	}
}

Menu::~Menu()
{
	delete &this->_canvas;
}

void Menu::addShape()
{
	int choice = 0;
	double x, y, x1, y1, x2, y2, radius, length, width;
	
	std::string name;

	std::cout << "Enter a 0 to add a circle." << std::endl;
	std::cout << "Enter 1 to add an arrow." << std::endl;
	std::cout << "Enter 2 to add a triangle. " << std::endl;
	std::cout << "Enter 3 to add a rectangle." << std::endl;
	std::cin >> choice;
	system("cls");

	if (choice < 0 || choice > EXIT)
	{
		do {
			system("cls");
			std::cout << "The number must be from 0 to 3!" << std::endl;
			std::cout << "Enter a 0 to add a circle." << std::endl;
			std::cout << "Enter 1 to add an arrow." << std::endl;
			std::cout << "Enter 2 to add a triangle. " << std::endl;
			std::cout << "Enter 3 to add a rectangle." << std::endl;
			std::cin >> choice;
			system("cls");
		} while (choice < 0 || choice > EXIT); //get which shape you want to add
	}

	std::cout << "Please Enter X:" << std::endl;
	std::cin >> x;
	std::cout << "Please Enter Y:" << std::endl; //every shape need an atleast one point
	std::cin >> y;

	switch (choice)
	{
	case 0: //circle

		
		std::cout << "Please Enter the radius:" << std::endl;
		std::cin >> radius;
		std::cout << "Please Enter the name of the shape:" << std::endl;
		std::cin >> name;
		this->shapes.push_back(new Circle(Point(x,y), radius, name, "Circle"));  
		system("cls");
		
		break;
	case 1: //arrow
		
		std::cout << "Please Enter X1:" << std::endl;
		std::cin >> x1;
		std::cout << "Please Enter Y1:" << std::endl;
		std::cin >> y1;
		std::cout << "Please Enter the name of the shape:" << std::endl;
		std::cin >> name;
		this->shapes.push_back(new Arrow(Point(x, y),Point(x1, y1), name, "Arrow"));
		system("cls");
		break;
	case 2: //triangle
		std::cout << "Please Enter X1:" << std::endl;
		std::cin >> x1;
		std::cout << "Please Enter Y1:" << std::endl;
		std::cin >> y1;
		std::cout << "Please Enter X2:" << std::endl;
		std::cin >> x2;
		std::cout << "Please Enter Y2:" << std::endl;
		std::cin >> y2;
		std::cout << "Please Enter the name of the shape:" << std::endl;
		std::cin >> name;
		system("cls");
		this->shapes.push_back(new Triangle(Point(x, y), Point(x1, y1),Point(x2, y2), name, "Triangle"));
		break;
	case 3: //rectangle

		std::cout << "Please Enter the length of the shape:" << std::endl;
		std::cin >> length;
		std::cout << "Please Enter the width of the shape:" << std::endl;
		std::cin >> width;
		std::cout << "Please Enter the name of the shape:" << std::endl;
		std::cin >> name;
		system("cls");
		this->shapes.push_back(new myShapes::Rectangle(Point(x, y), length, width, name, "Rectriangle"));
		break;
	default:
		break;
	}
}

void Menu::deleteAllShapes()
{
	for (int i = 0; i < this->shapes.size(); i++)
	{
		this->shapes[i]->clearDraw(_canvas); //clearing the canves
	}
	this->shapes.resize(0); //shrinking the vecotr
	
}

void Menu::show()
{
	int choice = 0, choice1 = 0;
	Shape* ptr; //to save the shape at move
	do {
		for (int i = 0; i < this->shapes.size(); i++)
		{
			std::cout << "Enter " << i << " for " << this->shapes[i]->getName() << "(" << this->shapes[i]->getType() << ")" << std::endl;
		}
		std::cin >> choice;
	} while (choice < 0 || choice > this->shapes.size()); //get which shape you want to modifay


	do
	{
		system("cls");
		std::cout << "Enter 0 to move the shape" << std::endl;
		std::cout << "Enter 1 to get it's details." << std::endl;
		std::cout << "Enter 2 to remove this shape." << std::endl;
		std::cin >> choice1;
		system("cls");
	} while (choice1 < 0 || choice1 > RMV_CHOICE); //get which function you want to do to the current shape

	switch (choice1)
	{
	case MOV:
		double x, y;
		std::cout << "Please enter the X moving scale:" << std::endl;
		std::cin >> x;
		std::cout << "Please enter the Y moving scale:" << std::endl;
		std::cin >> y;
		this->shapes[choice]->clearDraw(this->_canvas); //clearing the current shape then moving her
		this->shapes[choice]->move(Point(this->shapes[choice]->getX() + x, this->shapes[choice]->getY() + y));
		ptr = this->shapes[choice]; 
		this->shapes[choice]->clearDraw(this->_canvas);
		this->shapes[choice] = ptr;
		system("cls");
		for (int i = 0; i < this->shapes.size(); i++)
		{
			this->shapes[i]->draw(this->_canvas); 
		}
		break;
	case DETAILS:
		this->shapes[choice]->printDetails();
		break;
	case RMV_CHOICE:
		this->shapes[choice]->clearDraw(this->_canvas);
		for (int i = choice; i < this->shapes.size() - 1; i++)
		{
			this->shapes[i] = this->shapes[i + 1];
		}
		for (int i = 0; i < this->shapes.size(); i++)
		{
			this->shapes[i]->draw(this->_canvas);
		}
		break;
	default:
		break;
	}
	

}




