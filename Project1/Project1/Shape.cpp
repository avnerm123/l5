#include "Shape.h"
#include <iostream>
Shape::Shape(const std::string& name, const std::string& type)
{
	this->_name = name;
	this->_type = type;
}
void Shape::printDetails() const
{
	std::cout << "The name of the shape is: " << this->_name << std::endl << "The type of the shape is: " << this->_type << std::endl;
	std::cout << "The Area of the shape is: " << this->getArea() << std::endl << "The perimeter of the shape is: " << this->getPerimeter() << std::endl;
	getchar();
	getchar();
}

std::string Shape::getType() const
{
	return this->_type;
}

std::string Shape::getName() const
{
	return this->_name;
}


